#include <iostream>

int main()
{
	char S[255];
	std::cout << "S: ";
	std::cin.getline(S, sizeof(S));
	int N;
	std::cout << "N: ";
	std::cin >> N;
	const int x = (strlen(S) - 1) * N + strlen(S);
	char M[x];
	int i = 0;
	int a = 0;
	while (i < x)
	{
		if (i % (N+1) == 0)
		{
			M[i] = S[a];
			a++;
		}
		else
		{
			M[i] = '*';
		}
		i++;
	}
	std::cout << M;
	return 0;
}
