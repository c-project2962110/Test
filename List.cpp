#include <iostream>
using namespace std;

class element
{
public:
	string value;
	element* next_element = nullptr;
};

class List
{
	element* first_element = nullptr;
	element* last_element = nullptr;
public:
	int findElement(string k)
	{
		element *p = first_element;
		int count = 0;
		while (p != nullptr)
		{
			if (p -> value == k) return count;
			else 
			{
				count++;
				p = p -> next_element;
			}
		}
		return -1;
	}
	void addElement(string k)
	{
		if (first_element == nullptr)
		{
			first_element = new element;
			first_element -> value = k;
			last_element = first_element;
		}
		else
		{
			last_element -> next_element = new element;
			last_element -> next_element -> value = k;
			last_element = last_element -> next_element; 
		}
	}
	void deleteElement (int n)//n - індекс елемента списка, який потрібно видалити
	{
		element* ptr = first_element;
		element* ptr1 = first_element;
		int count = 0;
		while (ptr -> next_element != nullptr)
		{
			if (count == n) 
			{
				if (n == 0) first_element = first_element -> next_element;	
				else ptr1 -> next_element = ptr -> next_element;
				delete ptr;
				return;	
			}
			else 
			{
				ptr1 = ptr;
				ptr = ptr -> next_element;
				count++;
			}
			
		}
		cout << "There are less than " << n+1 << "elements in the list!\n";
	}
	void printList()
	{
		element* ptr = first_element;
		while (ptr != nullptr)
		{
			cout << ptr -> value << ' ';
			ptr = ptr -> next_element;
		}
		cout << '\n';
	}
	void reversePrint()
	{
		element* ptr = nullptr;
		element* p = first_element;
		int a = false;
		while (a != true)
		{
			if (p -> next_element == ptr)
			{
				cout << p -> value << ' ';
				ptr = p;
				p = first_element;
				if (ptr == p -> next_element)
				{
					cout << p -> value << ' ';
					a = true;
				}
			}
			else p = p -> next_element;
		}
		cout << '\n';
	}
};

int main()
{
	List languages;
    languages.addElement("Python");
    languages.addElement("Java");
    languages.addElement("C++");
    languages.printList();
    cout << languages.findElement("Python") << ' ' << languages.findElement("Java") << ' ' << languages.findElement("C++") << '\n';
	cout << '\n';
	languages.reversePrint();
	cout << '\n';
    languages.deleteElement(0);
    languages.printList();
    cout << '\n';
    languages.reversePrint();
	return 0;
}
