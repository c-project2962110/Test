#include <iostream>
#include <set>

int count(std::set <char> A, char str[])
{
	int a = 0;
	for (int i = 0; i < strlen(str); i++)
	{
		if (A.count(str[i]) == 1) a++;
	}
	return a;
}
int main()
{
	std::set <char> A = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	
	char str[255];
	std::cin.getline(str, sizeof(str));
	
	int a = count(A, str);
	std::cout << a;
	
	return 0;
}
