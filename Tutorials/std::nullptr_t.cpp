/*std::nullptr_t - це новий тип даних, доданий у С++ 11 і який може мати лише значення nullptr.
На перший погляд це здається безглуздим, але може бути корисним у випадку, коли потрібно написати
функцію, яка приймає аргумент nullptr.*/

#include <iostream>
#include <cstddef> // для std::nullptr_t
 
void doAnything(std::nullptr_t ptr)
{
    std::cout << "in doAnything()\n";
}
 
int main()
{
    doAnything(nullptr); // виклик функції doAnything() з аргументом типу std::nullptr_t
 
    return 0;
}
