//Ця програма є прикладом використання BFS(пошуку в ширину). Основна ідея BFS полягає в тому, щоб спочатку дослідити всі вершини, що 
//безпосередньо зв'язані з початковою вершиною, а потім поступово розширювати область пошуку, досліджуючи вершини, які знаходяться на
//відстані одного ребра від вже відвіданих вершин, а потім двох ребер і так далі.


#include <unordered_map>
#include <iostream>
#include <queue>
#include <set>

using namespace std;

bool is_seller(string a)
{
	if (a[0] == 'm') return true;
	else return false;
}

int main()
{
	set <string> checked_people; //створюємо множину для того, щоб не перевіряти одну і туж саму людину декілька разів 
	// і цим самим запобігти зацикленню.
	unordered_map <string, vector<string>> graph;
	
	graph["you"] = {"alice", "bob", "claire"};
	graph["bob"] = {"anuj", "peggy"};
	graph["alice"] = {"peggy"};
	graph["claire"] = {"thom", "jonny"};
	graph["anuj"] = {};
	graph["peggy"] = {};
	graph["thom"] = {};
	graph["jonny"] = {};
	
	queue<string> search_queue;
	for (int i = 0; i < graph["you"].size(); i++) search_queue.push(graph["you"][i]);
	
	while(!search_queue.empty())
	{
		string person = search_queue.front();
		search_queue.pop();
		if (is_seller(person))
		{
			cout << person << " is a mango seller!\n";
			return 0;
		}
		else if (!checked_people.count(person))
		{
			for (int _i = 0; _i < graph[person].size(); _i++) search_queue.push(graph[person][_i]);
			checked_people.insert(person);
		}
	}
	cout << "there are no mango sellers!\n";
	return 0;
}
