#include <iostream>
#include <unordered_map>
using namespace std;
int main()
{
	unordered_map<string, int> phone_book;
	phone_book["emergency"] = 911;
	//Ось деякі функції для роботи з хеш-таблицею:
	bool x = phone_book.count("shariks");// - перевіряє наявність ключа у хеш - таблиці
	//phone_book.erase("emergency"); // - видаляє елемент з хеш-таблиці за ключем 
	int size = phone_book.size(); // - Повертає кількість пар ключ - елемент у хеш-таблиці
	bool y = phone_book.empty(); // - повертає true, якщо хеш-таблиця порожня чи false, якщо хеш-таблиця непорожня
	//phone_book.clear(); // - видаляє усі елементи з хеш - таблиці
	int size_ = 7;
	phone_book.reserve(size_); // - виділяє для хеш-таблиці пам'ять для певної кількості елементів(зручно при великих
	//обсягах даних
	//Ітерація по елементах хеш-таблтці здійснюється за допомогою цикла for наступним чином:
	for (auto it = phone_book.begin(); it != phone_book.end(); ++it){
        std::cout << "Key: " << it->first << ", Value: " << it->second << std::endl;
    } // важливо зазначити, що в цьому випадку змінна it набуває наступного типу даних:
    //std::unordered_map<std::string, int>::iterator, але замість цього можна використати ключове слово auto
	return 0;
}
