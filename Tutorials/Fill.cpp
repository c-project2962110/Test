#include <iostream>

//Команда fill дозволяє присвоювати значення одразу декільком елементам одразу.
//Синатксис виглядає наступним чином: fill(початок, кінець, значення, яке присвоюється)

int main()
{
	std::string food [15];
	
	fill(food + 3, food + 7, "pizza");
	fill (food, food + 3, "burger");
	fill (food + 7, food + 15, "ice cream");
	
	for(std::string item : food)
	{
		std::cout << item << '\n';
	}
	return 0;
}
