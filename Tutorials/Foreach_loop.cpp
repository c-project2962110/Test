//Цикл foreach забезпечує простіший доступ до елементів масиву

#include <iostream>

int main()
{
    std::string students[] = {"Patrick", "Chad", "Squidward", "Missy"};

    for(std::string student : students)
    //Де std::string - тип даних елементів масиву, student - назва змінної, students - назва масиву
    {
        std::cout << student << std::endl;
    }
    return 0;
}
