#include <iostream>

enum class Colors
{
	GREEN,
	YELLOW,
	RED
};
int main()
{
	int a;
	std::cin >> a;
	if(a == static_cast <int> (Colors::GREEN)) std::cout << "green" << std::endl;
	if(a == static_cast <int> (Colors::YELLOW)) std::cout << "yellow" << std::endl;
	if(a == static_cast <int> (Colors::RED)) std::cout << "red" << std::endl;
	return 0;
}