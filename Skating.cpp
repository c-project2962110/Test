#include <iostream>
#include <iomanip>
using namespace std;
void skating(int n, double& k)
{
	int b[n];
	int s = 0, min = 101, max = -1;
	for (int _i = 0; _i < n; _i++)
	{
		int t; cin >> t;
		*(b + _i) = t; 
		s += t;
		if (t < min) min = t;
		if (t > max) max = t; 
	}
	s -= (min + max);
	double q = static_cast<double>(s) / (n - 2);
	k = q;
}
int main()
{
	int n, m; cin >> n >> m;
	double a[m];
	for (int i = 0; i < m; i++)
	{
		double t;
		double& k = t;
		skating(n, k);
		*(a + i) = t;
	}
	for (int i = 0; i < m; i++)
	{
		cout << setprecision(4);
		cout << *(a + i) << ' ';
	}
	return 0;
}
