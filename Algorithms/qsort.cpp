#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
using namespace std;

int getRandomNumber(int min, int max)
{
	return min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
}
vector<int> quick_sort(vector<int>vec)
{
	if (vec.size() < 2) return vec;
	else
	{
		vector<int>less;
		vector<int>greater;
		int pivot = vec[0];
		for (int i = 1; i < vec.size(); i++)
		{
			if(vec[i] >= pivot) greater.push_back(vec[i]);
			else less.push_back(vec[i]);
		}	
		vector<int>vec1;
		less = quick_sort(less);
		greater = quick_sort(greater);
		for (int i = 0; i < less.size(); i++) vec1.push_back(less[i]);
		vec1.push_back(pivot);
		for (int i = 0; i < greater.size(); i++) vec1.push_back(greater[i]);
		return vec1;
	}
	
}

int main()
{
	srand(static_cast<unsigned int>(time(0)));
	int size; 
	cout << "size: "; 
	cin >> size;
	vector<int> vec;
	for (int i = 0; i < size; i++)
	{
		int t = getRandomNumber(0, 100);
		vec.push_back(t);
	}
	vec = quick_sort(vec);
	for (int i = 0; i < size; i++) cout << vec[i] << ' ';
	return 0;
}