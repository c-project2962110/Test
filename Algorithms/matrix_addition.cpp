#include <iostream>

int getRandomNumber(int min, int max)
{
    return min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
}

void printMatrix(double matrix[5][6])
{
	for (int i = 0; i < 5; i++)
	{
		for (int _i = 0; _i < 6; _i++)
		{
			std::cout << matrix[i][_i] << " ";
		}
		std::cout << "\n";
	}
}

void addition (double matrix1[5][6], double matrix2[5][6], double matrix[5][6])
{
	for (int i = 0; i < 5; i++)
	{
		for (int _i = 0; _i < 6; _i++)
		{
			matrix[i][_i] = matrix1[i][_i] + matrix2[i][_i];
		}
	}
}

int main()
{
	int min = 0;
	int max = 10000;
	double matrix1 [5][6];
	for (int i = 0; i < 5; i++)
	{
		for (int _i = 0; _i < 6; _i++)
		{
			matrix1[i][_i] = getRandomNumber(min, max);
		}
	}
	printMatrix(matrix1);
	std::cout << "\n";
	double matrix2 [5][6];
	for (int i = 0; i < 5; i++)
	{
		for (int _i = 0; _i < 6; _i++)
		{
			matrix2[i][_i] = getRandomNumber(min, max);
		}
	}
	printMatrix(matrix2);
	std::cout << "\n";
	double matrix3 [5][6];
	addition(matrix1, matrix2, matrix3);
	printMatrix(matrix3);
	
	return 0;
}
