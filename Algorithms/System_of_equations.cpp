#include <iostream>
#include <iomanip>

void triangulate( double matrix [5][6])
{
	std::cout << "System:" << '\n';
	for (int i = 0; i < 5; i++)
	{
		for (int _i = 0; _i < 6; _i++)
		{
			std::cout << std::setprecision(3) << matrix[i][_i] << " ";
		}
		std::cout << "\n";
	}
	std::cout << '\n';
	int x = 0;
	int m = 0;
	while (x < 5)
	{
		double a = matrix[x][x];
		double b;
		for (int i = x; i < 6; i++)
		{
			matrix[x][i] = matrix[x][i] / a;
		}
		for (int i = x + 1; i < 5; i++)
		{
			b = matrix[i][m];
			for (int _i = x; _i < 6; _i++)
			{
				matrix[i][_i] = matrix[i][_i] - matrix[x][_i] * b;
			}
		}
		x++;
		m++;
	}
	
	x = 5;
	int y = x - 1;
	while (x > 0)
	{
		int i;
		for (i = 1; i <= y; i++)
		{
			matrix[y - i][5] -= matrix[y][5] * matrix[y - i][x - 1];
			matrix[y - i][x - 1] = 0;
		}
		
		x--;
		y--;
	}
	std::cout << "Solution:\n";
	for (int i = 0; i < 5; i++)
	{
		for (int _i = 0; _i < 6; _i++)
		{
			std::cout << std::setprecision(3) << matrix[i][_i] << " ";
		}
		std::cout << "\n";
	}
}

int main()
{
	double matrix [5][6] =
	{
		{1, 4, 1, 1, 1, 15},
		{2, 2, 2, 2, 2, 12},
		{1, 1, 3, 2, 2, 18},
		{4, 43, 3, 4, 3, 20},
		{22, 2, 22, 2, 7, 14},
	};
	triangulate(matrix);
	
	return 0;
}
