#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;
int randomNumber(int min, int max)
{
	return min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
}
vector<int> quick_sort(vector<int> vec)
{
	if (vec.size() < 2) return vec;
	else
	{
		vector<int> less;
		vector<int> greater;
		int pivot = vec[0];
		for (int i = 1; i < vec.size(); i++)
		{
			if (vec[i] <= pivot) less.push_back(vec[i]);
			else greater.push_back(vec[i]);
		} 
		vector<int> res;
		greater = quick_sort(greater);
		less = quick_sort(less);
		
		for (int i = 0; i < less.size(); i++) 
		{
			res.push_back(less[i]);
		}
		res.push_back(pivot);
		for (int i = 0; i < greater.size(); i++) 
		{
			res.push_back(greater[i]);
		}
		return res;
	}
}
void matrix_qsort(int matrix[10][10], int cols, int rows)
{
	int pivot = matrix[0][0];
	vector<int> less;
	vector<int> greater;
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			if (matrix[i][_i] <= pivot) less.push_back(matrix[i][_i]);
			else greater.push_back(matrix[i][_i]);
		}
	}
	less = quick_sort(less);
	greater = quick_sort(greater);
	int m = 0, n = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			if (m < less.size())
			{
				matrix[i][_i] = less[m];
				m++;
			}
			else 
			{
				matrix[i][_i] = greater[n];
				n++;
			}
		}
	}
	for (int i = 1; i < rows; i += 2)
	{
		for (int _i = 0; _i <= (cols - 1) / 2; _i++)
		{
			swap(matrix[i][_i], matrix[i][cols - _i - 1]);
		}
	}
}
int main()
{
	srand(static_cast<unsigned int>(time(0)));
	ios_base::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	int min = 100, max = 1000, cols = 10, rows = 10;
	int matrix[10][10];
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++) matrix[i][_i] = randomNumber(100, 1000);
	}
	matrix_qsort(matrix, cols, rows);
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++) cout << matrix[i][_i] << ' ';
		cout << '\n';
	}
	return 0;
}
