#include <iostream>
#include <ctime>
using namespace std;

int getRandomNumber (int min, int max)
{
	return min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
}

void matrixSort(int a[100][100], int cols, int rows)
{
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			int min = 1001;
			int min_q;
			int min__q;
			int _q, q;
			for (q = 0; q < rows; q++)
			{
				for (_q = 0; _q < cols; _q++)
				{
					if (100*(q+1) + _q > 100 * (i+1) + _i)
					{
						if (i != 0 || _i != 0)
						{
							if (_i == 0) 
							{
								if (a[q][_q] <= min && a[q][_q] >= a[i-1][cols - 1])
								{
									min = a[q][_q];
									min_q = q;
									min__q = _q;
								}	
							}
							else
							{
								if (a[q][_q] <= min && a[q][_q] >= a[i][_i - 1])
								{
									min = a[q][_q];
									min_q = q;
									min__q = _q;	
								}
							}
						}
						else if (a[q][_q] <= min)
						{
							min = a[q][_q];
							min_q = q;
							min__q = _q;
						}
					}
					
				}
			}
			//cout << a[min_q][min__q] << '\n';
			if (a[min_q][min__q] <= a[i][_i]) swap(a[i][_i], a[min_q][min__q]);
		}
	}
	for (int i = 1; i < rows; i += 2)
	{
		for (int _i = 0; _i <= (cols - 1) / 2; _i++)
		{
			swap(a[i][_i], a[i][cols - _i - 1]);
		}
	}
}   
           
int main()
{
	srand(static_cast<unsigned int>(time(0)));
	int cols = 100;
	int rows = 100;
	int matrix[100][100];
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			matrix[i][_i] = getRandomNumber(100, 1000);
		}
	}
	cout << "Initial matrix: \n";
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			cout << matrix[i][_i] << ' ';
		}
		cout << '\n';
	}
	matrixSort(matrix, cols, rows);

	cout << '\n';
	cout << "Sorted matrix:\n";
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			cout << matrix[i][_i] << ' ';
		}
		cout << '\n';
	}
	cout << '\n';
}
