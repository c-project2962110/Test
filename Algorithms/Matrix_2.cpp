#include <iostream>

int** changeMatrix(int** matrix, int n)
{
    int** _matrix = new int*[n];
    for (int i = 0; i < n; i++)
    {
        _matrix[i] = new int[n];
        for (int _i = 0; _i < n; _i++)
        {
            _matrix[i][_i] = matrix[_i][i];
        }
    }
    return _matrix;
}

void printMatrix(int **matrix, const int n )
{
	for (int i = 0; i < n; i++)
	{
		for (int _i = 0; _i < n; _i++)
		{
			std::cout << matrix[i][_i] << " ";
		}
		std::cout << "\n";
	}
}
int main()
{
	const int n = 5;
    int** matrix = new int*[n];
    int a = 0;
    for (int i = 0; i < n; i++)
    {
        matrix[i] = new int[n];
        for (int _i = 0; _i < n; _i++)
        {
            matrix[i][_i] = a;
            a++;
        }
    }
	printMatrix(matrix, n);
	matrix = changeMatrix(matrix, n);
	std::cout << "\n";
	printMatrix(matrix, n);
	
	return 0;
}
