#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
using namespace std;

int getRandomNumber (int min, int max)
{
	return min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
}

void matrixSort(int a[100][100], int cols, int rows)
{
	int array[cols * rows];
	int q = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			array[q] = a[i][_i];
			q++;
		}
	}
	sort (array, array + cols * rows);
	
	//for (int i = 0; i < q; i++) cout << array[i] << ' ';
	
	q = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			a[i][_i] = array[q];
			q++;
		}
	}
	
	for (int i = 1; i < rows; i += 2)
	{
		for (int _i = 0; _i <= (cols - 1) / 2; _i++)
		{
			swap(a[i][_i], a[i][cols - _i - 1]);
		}
	}
}

int main()
{
	srand(static_cast<unsigned int>(time(0)));
	int cols = 100;
	int rows = 100;
	int matrix[100][100];
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			matrix[i][_i] = getRandomNumber(100, 1000);
		}
	}
	matrixSort(matrix, cols, rows);
	cout << '\n';
	cout << '\n';
	for (int i = 0; i < rows; i++)
	{
		for (int _i = 0; _i < cols; _i++)
		{
			cout << matrix[i][_i] << ' ';
		}
		cout << '\n';
	}
}
