#include <iostream>
#include <cstring>
#include <cmath>

int eliminate_unset_bits(char A[])
{
	int x = strlen(A);

	int a = 0;
	for (int i = 0; i < x; i++)
	{
		if (A[i] == '0')
		{
			a++;
		}
	}
	a = x - a;
	//std::cout << a << std::endl;
	int s = 0;
	for (int i = 0; i < a; i++)
	{
		s = s + pow(2, i);
	}
	return s;
	
}

/*int main()
{
	char A[200];
	std::cin >> A;
	int s = eliminate_unset_bits(A);
	std::cout << s;
	return 0;
}*/
