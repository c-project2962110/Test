#include <iostream>
#include <algorithm>

enum animals
{
	DEFAULT = -1,
	ZEBRA, 
	TIGER, 
	LIZARD,
	MONKEY,
	BEAR,
	GIRAFFE,
	ANTELOPE,
	WOLF,
	LION
};

struct animal
{
	animals kind = DEFAULT;
	std::string name;
	unsigned short age;
};

struct Zoo
{
	std::string address;
	animal animal_1;
	animal animal_2;
	animal animal_3;
};

struct city
{
	std::string name;
	Zoo zoo_1;
	Zoo zoo_2;
};

void zooList(std::string name, short zoo, city Wienna, city NYC, std::string array[], int size)
{
	if (name == "NYC")
	{
		if (zoo == 1)
		{
			std::cout << array[static_cast<int>(NYC.zoo_1.animal_1.kind)] << "\n";
			std::cout << array[static_cast<int>(NYC.zoo_1.animal_2.kind)] << "\n";
			std::cout << array[static_cast<int>(NYC.zoo_1.animal_3.kind)] << "\n";	
		}	
		else
		{
			std::cout << array[static_cast<int>(NYC.zoo_2.animal_1.kind)] << "\n";
			std::cout << array[static_cast<int>(NYC.zoo_2.animal_2.kind)] << "\n";
			std::cout << array[static_cast<int>(NYC.zoo_2.animal_3.kind)] << "\n";	
		}
	}
	if (name == "Wienna")
	{
		if (zoo == 1)
		{
			std::cout << array[static_cast<int>(Wienna.zoo_1.animal_1.kind)] << "\n";
			std::cout << array[static_cast<int>(Wienna.zoo_1.animal_2.kind)] << "\n";
			std::cout << array[static_cast<int>(Wienna.zoo_1.animal_3.kind)] << "\n";	
		}	
		else
		{
			std::cout << array[static_cast<int>(Wienna.zoo_2.animal_1.kind)] << "\n";
			std::cout << array[static_cast<int>(Wienna.zoo_2.animal_2.kind)] << "\n";
			std::cout << array[static_cast<int>(Wienna.zoo_2.animal_3.kind)] << "\n";	
		}
	}	
}
void findAnimal(city Cities[], int array, int b)
{

	for (int i = 0; i < array; i++)
	{
		if (static_cast<int>(Cities[i]. zoo_1. animal_1. kind) == b)
		{
			std::cout << "The animal is in the city of " << Cities[i].name << ", the first zoo, enclosure num.1 \n";
		}
		if (static_cast<int>(Cities[i]. zoo_1. animal_2. kind) == b)
		{
			std::cout << "The animal is in the city of " << Cities[i].name << ", the first zoo, enclosure num.2 \n";
		}
		if (static_cast<int>(Cities[i]. zoo_1. animal_3. kind) == b)
		{
			std::cout << "The animal is in the city of " << Cities[i].name << ", the first zoo, enclosure num.3 \n";
		}
		//Перевірка першого зоопарку міста
		if (static_cast<int>(Cities[i]. zoo_2. animal_1. kind) == b)
		{
			std::cout << "The animal is in the city of " << Cities[i].name << ", the second zoo, enclosure num.1 \n";
		}
		if (static_cast<int>(Cities[i]. zoo_2. animal_2. kind) == b)
		{
			std::cout << "The animal is in the city of " << Cities[i].name << ", the second zoo, enclosure num.2 \n";
		}
		if (static_cast<int>(Cities[i]. zoo_2. animal_3. kind) == b)
		{
			std::cout << "The animal is in the city of " << Cities[i].name << ", the second zoo, enclosure num.3 \n";
		}
		//Перевірка другого зоопарку міста
		
		//Примітка: програма виводить не номер зоопарку загалом, а порядковий номер серед зоопарків конкретного 
		//міста.
	}
}

std::string theOldest (city City, int& q)
{
	
	int Animal_1[] = {City.zoo_1.animal_1.age, City.zoo_1.animal_2.age, City.zoo_1.animal_3.age};
	std::sort (Animal_1, Animal_1 + sizeof(Animal_1) / sizeof(int));
	int Animal_2[] = {City.zoo_2.animal_1.age, City.zoo_2.animal_2.age, City.zoo_2.animal_3.age};
	std::sort (Animal_2, Animal_2 + sizeof(Animal_2) / sizeof(int));
	
	if(Animal_1[2] >= Animal_2[2])
	{
		q = Animal_1[2];
		return "zoo number 1";
	}
	else
	{
		q = Animal_2[2];
		return "zoo number 2";
	}
}
int main()
{
	animal zebra = {ZEBRA, "Missy", 17};
	animal tiger = {TIGER, "Georgie", 25};
	animal lizard = {LIZARD, "Sheldon", 8};
	
	animal monkey = {MONKEY, "Oliver", 10};
	animal bear = {BEAR, "Ryan", 14};
	animal giraffe = {GIRAFFE, "Cameron", 22};
	
	animal antelope = {ANTELOPE, "Susy", 12};
	animal wolf = {WOLF, "Oliver", 11};
	animal lion = {LION, "David", 14};
	
	Zoo zoo_1 = {"2300 Southern Boulevard, Bronx", zebra, tiger, lizard};
	Zoo zoo_2 = { "Maxingstrasse 13b, Wien,", monkey, bear, giraffe};
	Zoo zoo_3 = {"East 64th Street, New York,", antelope, wolf, lion};
	
	
	city NYC = {"New York City", zoo_1, zoo_3};
	city Wienna = {"Wienna", zoo_2};
	
	city Cities[] = {NYC, Wienna};
	
	std::string a;
	std::cout << "Please, enter an animal you would like to find(the first letter must be capital): ";
	std::cin >> a;
	
	int b;
	

	if (a == "Zebra")
		b = 0; 
	else if (a == "Tiger")
		b = 1;
	else if (a == "Lizard")
		b = 2;
	else if (a == "Monkey") 
		b = 3;
	else if (a == "Bear")
		b = 4;
	else if (a == "Giraffe") 
		b = 5;
	else if (a == "Antelope")
		b = 6;
	else if (a == "Wolf")
		b = 7;
	else if (a == "Lion")
		b = 8;
	else
	{
		b = 9;
		std::cout << "There is no such an animal as " << a << '\n';
	}
	
	std::string array[] = {"Zebra", "Tiger", "Lizard", "Monkey", "Bear", "Giraffe", "Antelope", "Wolf", "Lion"};	
	if (b != 9) findAnimal(Cities, sizeof(Cities) / sizeof(city), b);
	int m = 0;
	int &q1 = m;
	int &q2 = m;
	
	std::string theOldestAnimal_1 = theOldest(NYC, q1);
	std::string theOldestAnimal_2 = theOldest(Wienna, q2);
	
	std::cout << '\n';
	
	if (q1 >= q2) std::cout << "The oldest animal lives in NYC, " << theOldestAnimal_1 << ".\n";
	else std::cout << "The oldest animal lives in Wienna, zoo number " << theOldestAnimal_2 << ".\n";
	
	
	std::string h;
	std::cout << "Enter a name of a city(to get a list of animals, available NYC or Wienna): ";
	std::cin >> h;
	std::cout << "Enter a number of a zoo: ";
	short f;
	std::cin >> f;
	
	zooList(h, f, Wienna, NYC, array, sizeof(array) / sizeof(std::string));
	return 0;
}

