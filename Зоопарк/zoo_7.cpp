#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
vector<string>array = {"Zebra", "Tiger", "Lizard", "Monkey", "Bear", "Giraffe", "Antelope", "Wolf", "Lion"};
enum animals
{
	DEFAULT = -1,
	ZEBRA, 
	TIGER, 
	LIZARD,
	MONKEY,
	BEAR,
	GIRAFFE,
	ANTELOPE,
	WOLF,
	LION
};
struct animal
{
	animals kind = DEFAULT;
	string name;
	unsigned short age;
};
struct Zoo
{
	string address;
	vector<animal> box;
};
struct city
{
	string name;
	vector<Zoo> zoos;
};

animal zebra = {ZEBRA, "Missy", 17};
animal tiger = {TIGER, "Georgie", 25};
animal lizard = {LIZARD, "Sheldon", 8};
	
animal monkey = {MONKEY, "Oliver", 10};
animal bear = {BEAR, "Ryan", 14};
animal giraffe = {GIRAFFE, "Cameron", 22};
	
animal antelope = {ANTELOPE, "Susy", 12};
animal wolf = {WOLF, "Oliver", 11};
animal lion = {LION, "David", 14};
	
Zoo zoo_1 = {"2300 Southern Boulevard, Bronx", {zebra, tiger, lizard}};
Zoo zoo_2 = { "Maxingstrasse 13b, Wien,", {monkey, bear, giraffe}};
Zoo zoo_3 = {"East 64th Street, New York,", {antelope, wolf, lion}};
	
city NYC = {"NYC", {zoo_1, zoo_3}};
city Vienna = {"Vienna", {zoo_2}};
vector<city>Cities = {NYC, Vienna};

void zooList(string name, short zoo)
{
	zoo--;
	for (int i = 0; i < Cities.size(); i++)
	{
		if (Cities[i].name == name)
		for (int _i = 0; _i < NYC.zoos[zoo].box.size(); _i++) cout << array[static_cast<int>(NYC.zoos[zoo].box[_i].kind)] << "\n"; 
	}
}

void findAnimal(vector<city>Cities, int b)
{
	for (int i = 0; i < Cities.size(); i++)
	{
		for (int q = 0; q < Cities[i].zoos.size(); q++)
		{
			for (int _i = 0; _i < Cities[i].zoos[q].box.size(); _i++)
			{
				if (static_cast<int>(Cities[i].zoos[q].box[_i].kind) == b) cout << "The animal is in the city of " << Cities[i].name << ", zoo number "
				<< q+1 << ", enclosure num. " << _i+1 << '\n';
			}
		}
	}
}

int theOldest (int& q, int& f)//q - номер вольєру, f - номер міста в масиві
{
	vector<int> Animal_1 = {0};
	int zoo_indx, box_indx, max_age, city_indx;
	for (int u = 0; u < Cities.size(); u++)
	{
		for (int i = 0; i < Cities[u].zoos.size(); i++)
		{
			for (int _i = 0; _i < Cities[u].zoos[i].box.size(); _i++)
			{
				if (Cities[u].zoos[i].box[_i].age > max_age)
				{
					zoo_indx = i; 
					box_indx = _i;
					city_indx = u;
					max_age = Cities[u].zoos[i].box[_i].age;
				}
			}
		}
	}
	q = box_indx;
	f = city_indx;
	return zoo_indx;
}

bool MoveFromTo(string animall, string from, string to)
{
	bool x = false;
	animals j; //enum
	animal s; //struct
	for (int i = 0; i < array.size(); i++)
	{
		if (animall == array[i])
		{
			j = static_cast<animals>(i);
			break;
		}
	}
	for (int i = 0; i < Cities.size(); i++)
	{
		if (Cities[i].name == from)
		{
			for (int y = 0; y < Cities[i].zoos.size(); y++)
			{
				for (int _i = 0; _i < Cities[i].zoos[y].box.size(); _i++)
				{
					if (Cities[i].zoos[y].box[_i].kind == j) 
					{
						s = Cities[i].zoos[y].box[_i];
						Cities[i].zoos[y].box.erase(Cities[i].zoos[y].box.begin() + _i);
						x = true;
						break;
					}
				}
			
			}
		}
	}
	if (x != true)
	{
		cout << "There is no " << animall << " in " << from << '\n';
		return x;
	}
	for (int i = 0; i < Cities.size(); i++)
	{
		if (Cities[i].name == to)
		{
			Cities[i].zoos[0].box.push_back(s);
			return x;
		}
	}
	return 0;
}

void Buy(string kind, string name, int age)
{
	int t, i;
	bool flag = false;
	for (i = 0; i < array.size(); i++)
	{
		if (array[i] == kind) 
		{
			t = i; 
			flag = true;	
			i = array.size();
		}
	}
	if (flag == false) 
	{
		array.push_back(kind);
		t = i;
	}
	int m = 0;
	int& q = m, f = m;
	int zoo_indx = theOldest(q, f);
	Cities[f].zoos[zoo_indx].box[q].kind = static_cast<animals>(t);
	Cities[f].zoos[zoo_indx].box[q].age = age;
	Cities[f].zoos[zoo_indx].box[q].name = name;
	for (int i = 0; i < Cities[f].zoos[zoo_indx].box.size(); i++) cout << array[static_cast<int>(Cities[f].zoos[zoo_indx].box[i].kind)] << '\n'; 
}

int main()
{
	string a;
	cout << "Please, enter an animal you would like to find(the first letter must be capital): ";
	cin >> a;
	
	int b;
	bool flag = false;
	for (int i = 0; i < array.size(); i++)
	{
		if (array[i] == a)
		{
			b = i;
			i = array.size();
			flag = true;
		}
	}
	if (flag == true) findAnimal(Cities, b);
	else cout << "There is no such an animal as " << a << '\n';
	
	int o = 0;
	int& q = o, f = o;
	int zoo_indx = theOldest(q, f);
	cout << "The oldest animal lives in " << Cities[f].name <<", zoo number " << zoo_indx+1 << '\n';
	string h;
	cout << "Enter a name of a city(to get a list of animals, available NYC or Vienna): ";
	cin >> h;
	cout << "Enter a number of a zoo: ";
	short r;
	cin >> r;
	zooList(h, r);  
	cout << "Now, input next: Move <Animal> from <City1> to <City2>: ";
	string u;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	getline(cin, u);
	string animall;
	int i = 5;
	while (u[i] != ' ')
	{
		animall.push_back(u[i]);
		i++;	
	} 
	i += 6;
		
	string from;
	while (u[i] != ' ')
	{
		from.push_back(u[i]);
		i++;
	}
		
	i += 4;
	string to;
	while (i < u.size())
	{
		to.push_back(u[i]);
		i++;
	}
	bool y = MoveFromTo(animall, from, to);
	if (y == true)
	{
		for (int i = 0; i < Cities.size(); i++)
		{	
			if (Cities[i].name == to)
			{
				for (int _i = 0; _i < Cities[i].zoos[0].box.size(); _i++)
				{
					cout << array[Cities[i].zoos[0].box[_i].kind] << '\n';
				}
			}
		}
	}
	cout << "Now, you can buy a new animal instead of the oldest one you have.\n" 
		 << "Please, take into account, that the new animal will be put in the same place, where the oldest animal used to be.\n" 
		 << "Input next: Buy <Kind of animal>, aged <age>, named <name>(name's and kind's first letters must be capital): ";
	getline(cin, u);
	string kind, name, age_;
	int age;
	i = 4;
	while (u[i] != ',') 
	{
		kind.push_back(u[i]);
		i++;
	}
	i+=7;
	while (u[i] != ',')
	{
		age_.push_back(u[i]);
		i++;
	} 
	age = stoi(age_);
	i+=8;
	while (i < u.size())
	{
		name.push_back(u[i]);
		i++;
	}
	Buy(kind, name, age);
	return 0;
}
