#include <iostream>
#include <vector>

enum animals
{
	DEFAULT = -1,
	ZEBRA, 
	TIGER, 
	LIZARD,
	MONKEY,
	BEAR,
	GIRAFFE,
	ANTELOPE,
	WOLF,
	LION
};

struct animal
{
	animals kind = DEFAULT;
	std::string name;
	unsigned short age;
};

struct Zoo
{
	std::string address;
	animal animals[10];
};

struct city
{
	std::string name;
	Zoo zoo_1;
	Zoo zoo_2;
};

void findAnimal(city Cities[], int array, int b, int x)//x - кількість тварин в одному зоопарку
{
	for (int i = 0; i < array; i++)
	{
		for (int _i = 0; _i < x; _i++)
		{
			if (b == Cities[i].zoo_1.animals[_i].kind) 
			{
				std::cout << "The animal is in " << Cities[i].name << ", the first zoo, enclosure number " << _i + 1<<". \n";
			}
			if (b == Cities[i].zoo_2.animals[_i].kind) 
			{
				std::cout << "The animal is in " << Cities[i].name << ", the second zoo, enclosure number " << _i + 1 << ". \n";
			}
			
			
		}
	}
}

std::string theOldest(city City, int& q)
{
	int q1 = 0;
	int q2 = 0;
	for(int i = 0; i < sizeof(City.zoo_1.animals) / sizeof(animal); i++)
	{
		if (City.zoo_1.animals[i].age >= q1)
		{
			q1 = City.zoo_1.animals[i].age;
		}
	}
	for(int i = 0; i < sizeof(City.zoo_2.animals) / sizeof(animal); i++)
	{
		if (City.zoo_1.animals[i].age >= q1)
		{
			q2 = City.zoo_2.animals[i].age;
		}
	}
	
	if (q1 >= q2)  
	{
		q = q1;
		return "zoo number 1";
	}
	else
	{
		q = q2;
		return "zoo number 2";
	}
}

void zooList (std::string name, city NYC, city Wienna, std::string array[], int zoo)
{
	int x;
	if (name == "NYC" && zoo == 1)
	{
		x = sizeof(NYC.zoo_1.animals) / sizeof(animal);
		for (int i = 0; i < x; i++)
		{
			if (NYC.zoo_1.animals[i].kind != -1)
			{
				std::cout << array[static_cast<int>(NYC.zoo_1.animals[i].kind)] << '\n';
			}
		}
	}
	if (name == "NYC" && zoo == 2)
	{
		x = sizeof(NYC.zoo_2.animals) / sizeof(animal);
		for (int i = 0; i < x; i++)
		{
			if (NYC.zoo_2.animals[i].kind != -1)
			{
				std::cout << array[static_cast<int>(NYC.zoo_2.animals[i].kind)] << '\n';
			}
		}
	}
	if (name == "Wienna" && zoo == 1)
	{
		x = sizeof(Wienna.zoo_1.animals) / sizeof(animal);
		for (int i = 0; i < x; i++)
		{
			if (Wienna.zoo_1.animals[i].kind != -1)
			{
				std::cout << array[static_cast<int>(Wienna.zoo_1.animals[i].kind)] << '\n';
			}
		}
	}
	if (name == "Wienna" && zoo == 2)
	{
		x = sizeof(Wienna.zoo_2.animals) / sizeof(animal);
		for (int i = 0; i < x; i++)
		{
			if (Wienna.zoo_2.animals[i].kind != -1)
			{
				std::cout << array[static_cast<int>(Wienna.zoo_2.animals[i].kind)] << '\n';
			}
		}
	}
}
int main()
{
	animal zebra = {ZEBRA, "Missy", 17};
	animal tiger = {TIGER, "Georgie", 25};
	animal lizard = {LIZARD, "Sheldon", 8};
	
	animal monkey = {MONKEY, "Oliver", 10};
	animal bear = {BEAR, "Ryan", 14};
	animal giraffe = {GIRAFFE, "Cameron", 22};
	
	animal antelope = {ANTELOPE, "Susy", 12};
	animal wolf = {WOLF, "Oliver", 11};
	animal lion = {LION, "David", 14};
	
	Zoo zoo_1 = {"2300 Southern Boulevard, Bronx", {zebra, tiger, lizard}};
	Zoo zoo_2 = { "Maxingstrasse 13b, Wien,", {monkey, bear, giraffe}};
	Zoo zoo_3 = {"East 64th Street, New York,", {antelope, wolf, lion}};
	
	city NYC = {"New York City", zoo_1, zoo_3};
	city Wienna = {"Wienna", zoo_2};
	city Cities[] = {NYC, Wienna};
	
	std::string a;
	std::cout << "Please, enter an animal you would like to find(the first letter must be capital): ";
	std::cin >> a;
	int b;
	if (a == "Zebra")
		b = 0; 
	else if (a == "Tiger")
		b = 1;
	else if (a == "Lizard")
		b = 2;
	else if (a == "Monkey") 
		b = 3;
	else if (a == "Bear")
		b = 4;
	else if (a == "Giraffe") 
		b = 5;
	else if (a == "Antelope")
		b = 6;
	else if (a == "Wolf")
		b = 7;
	else if (a == "Lion")
		b = 8;
	else
	{
		b = 9;
		std::cout << "There is no such an animal as " << a << '\n';
	}
	if (b != 9) findAnimal(Cities, sizeof(Cities) / sizeof(city), b, 10);
	
	int m = 0;
	int& q1 = m;
	int& q2 = m;
	std::string theOldestAnimal_1 = theOldest(NYC, q1);
	std::string theOldestAnimal_2 = theOldest(Wienna, q2);
	
	
	if (q1 >= q2) std::cout << "The oldest animal lives in NYC, " << theOldestAnimal_1 << ".\n";
	else std::cout << "The oldest animal lives in Wienna, zoo number " << theOldestAnimal_2 << ".\n";
	
	std::string array[] = {"Zebra", "Tiger", "Lizard", "Monkey", "Bear", "Giraffe", "Antelope", "Wolf", "Lion"};
	
	std::cout << "Enter the name of the city, whose list of animals you'd like to receive(NYC or Wienna): ";
	std::string name;
	std::cin >> name;
	std::cout << "Enter the number of a zoo of the city: ";
	int zoo;
	std::cin >> zoo;
	zooList(name, NYC, Wienna, array, zoo);
	
	return 0;
}
