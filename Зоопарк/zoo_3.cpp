#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
enum animals
{
	DEFAULT = -1,
	ZEBRA, 
	TIGER, 
	LIZARD,
	MONKEY,
	BEAR,
	GIRAFFE,
	ANTELOPE,
	WOLF,
	LION
};
struct animal
{
	animals kind = DEFAULT;
	string name;
	unsigned short age;
};
struct Zoo
{
	string address;
	vector<animal> box;
};
struct city
{
	string name;
	Zoo zoo_1;
	Zoo zoo_2;
};
void zooList(string name, short zoo, city Wienna, city NYC, string array[], int size)
{
	if (name == "NYC" || name == "New York City")
	{
		if (zoo == 1) for (int i = 0; i < NYC.zoo_1.box.size(); i++) cout << array[static_cast<int>(NYC.zoo_1.box[i].kind)] << "\n";	
		else for (int i = 0; i < NYC.zoo_2.box.size(); i++) cout << array[static_cast<int>(NYC.zoo_2.box[i].kind)] << "\n";	
	}
	if (name == "Wienna")
	{
		if (zoo == 1) for (int i = 0; i < Wienna.zoo_1.box.size(); i++) cout << array[static_cast<int>(Wienna.zoo_1.box[i].kind)] << "\n";	
		else for (int i = 0; i < Wienna.zoo_2.box.size(); i++) cout << array[static_cast<int>(Wienna.zoo_2.box[i].kind)] << "\n";	
	}
		
}
void findAnimal(city Cities[], int array, int b)
{
	for (int i = 0; i < array; i++)
	{
		for (int _i = 0; _i < Cities[i].zoo_1.box.size(); _i++)
		{
			if (static_cast<int>(Cities[i]. zoo_1. box[_i]. kind) == b)
			{
				cout << "The animal is in the city of " << Cities[i].name << ", the first zoo, enclosure num." << _i+1 << '\n';
			}
		}
		//Перевірка першого зоопарку міста
		for (int _i = 0; _i < Cities[i].zoo_2.box.size(); _i++)
		{
			if (static_cast<int>(Cities[i]. zoo_2. box[_i]. kind) == b)
			{
				cout << "The animal is in the city of " << Cities[i].name << ", the second zoo, enclosure num." << _i+1 << '\n';
			}
		}
		//Перевірка другого зоопарку міста
		
		//Примітка: програма виводить не номер зоопарку загалом, а порядковий номер серед зоопарків конкретного 
		//міста.
	}
}
string theOldest (city City, int& q)
{
	vector<int> Animal_1 = {0};
	for (int i = 0; i < City.zoo_1.box.size(); i++) Animal_1.push_back(City.zoo_1.box[i].age); 
	sort (Animal_1.begin(), Animal_1.end());
	vector<int> Animal_2 = {0};
	for (int i = 0; i < City.zoo_2.box.size(); i++) Animal_2.push_back(City.zoo_2.box[i].age);
	sort (Animal_2.begin(), Animal_2.end());
	if(Animal_1[Animal_1.size() - 1] >= Animal_2[Animal_2.size() - 1])
	{
		q = Animal_1[Animal_1.size() - 1];
		return "zoo number 1";
	}
	else
	{
		q = Animal_2[Animal_2.size() - 1];
		return "zoo number 2";
	}
	
}
int MoveFromTo(string animall, string from, string to, string array[], int size, city cities[], int size1, bool &x)
{
	animals j; //enum
	animal s; //struct
	for (int i = 0; i < size; i++)
	{
		if (animall == array[i])
		{
			j = static_cast<animals>(i);
			break;
		}
	}
	for (int i = 0; i < size1; i++)
	{
		if (cities[i].name == from)
		{
			//cout << cities[i].name << '\n';
			for (int _i = 0; _i < cities[i].zoo_1.box.size(); _i++)
			{
				if (cities[i].zoo_1.box[_i].kind == j) 
				{
					//cout << cities[i].zoo_1.box[_i].kind << '\n';
					s = cities[i].zoo_1.box[_i];
					//cout << s.kind << '\n';
					cities[i].zoo_1.box.erase(cities[i].zoo_1.box.begin() + _i);
					x = true;
					break;
				}
			}
			for (int _i = 0; _i < cities[i].zoo_2.box.size(); _i++)
			{
				if (cities[i].zoo_2.box[_i].kind == j) 
				{
					s = cities[i].zoo_2.box[_i];
					cities[i].zoo_2.box.erase(cities[i].zoo_1.box.begin() + _i);
					x = true;
					break;
				}
			}
			
		}
	}
	//cout << s.kind << '\n';
	//cout << x << '\n';
	//cout << from << '\n';
	if (x != true)
	{
		cout << "There is no " << animall << " in " << from << '\n';
		return 0;
	}
	for (int i = 0; i < size1; i++)
	{
		if (cities[i].name == to)
		{
			//cout << cities[i].name << '\n';
			cities[i].zoo_1.box.push_back(s);
			return 0;
		}
	}
	return 0;
}
int main()
{
	animal zebra = {ZEBRA, "Missy", 17};
	animal tiger = {TIGER, "Georgie", 25};
	animal lizard = {LIZARD, "Sheldon", 8};
	
	animal monkey = {MONKEY, "Oliver", 10};
	animal bear = {BEAR, "Ryan", 14};
	animal giraffe = {GIRAFFE, "Cameron", 22};
	
	animal antelope = {ANTELOPE, "Susy", 12};
	animal wolf = {WOLF, "Oliver", 11};
	animal lion = {LION, "David", 14};
	
	Zoo zoo_1 = {"2300 Southern Boulevard, Bronx", {zebra, tiger, lizard}};
	Zoo zoo_2 = { "Maxingstrasse 13b, Wien,", {monkey, bear, giraffe}};
	Zoo zoo_3 = {"East 64th Street, New York,", {antelope, wolf, lion}};
	
	city NYC = {"NYC", zoo_1, zoo_3};
	city Wienna = {"Wienna", zoo_2};
	
	city Cities[] = {NYC, Wienna};
	
	string a;
	cout << "Please, enter an animal you would like to find(the first letter must be capital): ";
	cin >> a;
	
	int b;
	
	if (a == "Zebra")
		b = 0; 
	else if (a == "Tiger")
		b = 1;
	else if (a == "Lizard")
		b = 2;
	else if (a == "Monkey") 
		b = 3;
	else if (a == "Bear")
		b = 4;
	else if (a == "Giraffe") 
		b = 5;
	else if (a == "Antelope")
		b = 6;
	else if (a == "Wolf")
		b = 7;
	else if (a == "Lion")
		b = 8;
	else
	{
		b = 9;
		cout << "There is no such an animal as " << a << '\n';
	}
	
	string array[] = {"Zebra", "Tiger", "Lizard", "Monkey", "Bear", "Giraffe", "Antelope", "Wolf", "Lion"};	

	if (b != 9) findAnimal(Cities, sizeof(Cities) / sizeof(city), b);
	int m = 0;
	int g = 0;
	int &q1 = m;
	int &q2 = g;
	//cout << m << endl;
	string theOldestAnimal_1 = theOldest(NYC, q1);
	string theOldestAnimal_2 = theOldest(Wienna, q2);
	
	cout << '\n';
	
	if (q1 >= q2) cout << "The oldest animal lives in NYC, " << theOldestAnimal_1 << ".\n";
	else cout << "The oldest animal lives in Wienna, zoo number " << theOldestAnimal_2 << ".\n";
	
	string h;
	cout << "Enter a name of a city(to get a list of animals, available NYC or Wienna): ";
	cin >> h;
	cout << "Enter a number of a zoo: ";
	short f;
	cin >> f;
	zooList(h, f, Wienna, NYC, array, sizeof(array) / sizeof(string));
	cout << "Please, enter <stop> if you want to stop the program, or <move>, if you want to move an animal from one zoo to another: ";
	cin >> h;
	if (h == "stop") return 0;
	else
	{
		cout << "Now, input next: Move <Animal> from <City1> to <City2>: ";
		string u;
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		getline(cin, u);
		string animall;
		int i = 5;
		while (u[i] != ' ')
		{
			animall.push_back(u[i]);
			i++;	
		} 
		i += 6;
		
		string from;
		while (u[i] != ' ')
		{
			from.push_back(u[i]);
			i++;
		}
		
		i += 4;
		string to;
		while (i < u.size())
		{
			to.push_back(u[i]);
			i++;
		}
		//cout << to << '\n';
		bool x = false;
		bool &y = x;
		MoveFromTo(animall, from, to, array, 9, Cities, sizeof(Cities) / sizeof(city), y);
		if (y == true)
		{
			for (int i = 0; i < sizeof(Cities) / sizeof(city); i++)
			{	
				if (Cities[i].name == to)
				{
					for (int _i = 0; _i < Cities[i].zoo_1.box.size(); _i++)
					{
						cout << array[Cities[i].zoo_1.box[_i].kind] << '\n';
					}
				}
			}
		}
		
		//zooList(to, 1, Wienna, NYC, array, 9);
	}
	return 0;
}
