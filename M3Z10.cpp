#include <iostream>
#include <cmath>
using namespace std;
double Y(double a)
{
	int n = 1;
	double y = a;
	double x;
	for (int i = 2; i <= 15; i++)
	{
		x = a/i;
		x *= n;
		y += x;
		n *= -1;
	}
	return y;
}
int f(int a)
{
	int p = 1;
	while(a > 0)
	{
		p *= a;
		a--;
	}
	return p;
}
int main()
{

	int a; cin >> a;
	if (a%2 == 0 && a <= 10) cout << f(a);
	else if (sqrt(a) == static_cast<int>(sqrt(a))) cout << Y(a);	
	else cout << '7';
}
