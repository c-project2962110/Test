#include <iostream>
#include <vector>
using namespace std;
int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0); cout.tie(0);
	int n; cin >> n;
	vector<int> seq;
	for (int i = 0; i < n; i++)
	{
		int t; cin >> t;
		seq.push_back(t);
	}
	int max = seq[0], max1 = 0, max2;
	for (int i = 0; i < n; i++)
	{
		if (seq[i] > max)
		{
			max = seq[i];
			max1 = i;
			max2 = i;	
		} 
		else if (seq[i] == max) max2 = i;
	}
	//cout << max << '\n';
	if (max2 - max1 == 0) cout << '0';
	else cout << max2 - max1 - 1;
    return 0; 
}
